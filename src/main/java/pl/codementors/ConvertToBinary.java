package pl.codementors;

/**
 * Created by student on 22.05.17.
 */
public class ConvertToBinary {
    public static void main(String[] args){


        int number = 10;
        int[] numberArray = new int[4];
        int i = numberArray.length - 1;

/**
        do {
            int modulo = number % 2;
            numberArray[i] = modulo;
            i--;
            number /= 2;
        } while (number > 0);
*/
        for( int a=numberArray.length -1; a>=0; a-- ){
            int modulo = number % 2;
            numberArray[a] = modulo;
            number /=2;
        }
        for (int digit : numberArray) {
            System.out.print(digit + " ");
        }
        System.out.println();
    }

}
